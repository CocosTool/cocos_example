export default class GameConfig {
    private static _version: string = "200011015001";
    private static _is_dev: boolean = false;
    private static _is_skip_u8: boolean = false;
    private static _json_path = "game_config/game_config.json";
    public static init(callback?: Function){
        cc.loader.loadRes(this._json_path,cc.JsonAsset, (err, data) => {
            if(!err){
                const json = data.json;
                if("version" in json){
                    this._version = json.version;
                }
                if("is_dev" in json){
                    this._is_dev = json.is_dev;
                }
                if("is_skip_u8" in json){
                    this._is_skip_u8 = json.is_skip_u8;
                }
            }
            else{
                console.error("load game config error",err);
            }

            callback&&callback();
        });
    }
    public static get client_version() {
        return this._version;
    }

    public static get is_dev_test(){
        return this._is_dev;
    }

    public static get is_skip_u8(){
        return this._is_skip_u8;
    }
}
