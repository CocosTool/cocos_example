const fs = require("fire-fs");
const path = require("fire-path");
const config = {};
function onBuildStart(target, callback) {
    initPlugin();
    callback();
}

function onBeforeChangeFiles(target, callback) {
    callback();
}

function initPlugin() {
    let CfgUtil = Editor.require('packages://hot-update-tools/core/CfgUtil.js');
    CfgUtil.initCfg(function (data) {
        if (data) {
            config.isManifest = data.isManifest;
            config.autoPackVersion = data.autoPackVersion;
            config.packageUrl = data.packageUrl;
            config.manifestUUID = data.manifestUUID;
            Editor.log("[HotUpdateTools] build config:",config);
        }
    });
}

function onBeforeBuildFinish(target, callback){
    //添加热更新搜索路径
    addHotUpdateSearchPaths(target,()=>{
        autoReplaceManifest(target,callback);
    });
}
function autoReplaceManifest(target, callback){
    if (config.isManifest) {
        var dest = target.dest.replace(/\\/g,'/');
        var folder = dest.substring(dest.lastIndexOf('/'))+'/';
        var Generator = Editor.require('packages://hot-update-tools/core/version_generator.js');

        //version.txt本身没用,作用在于写入文件后构建完成会重新载入该插件,防止重新构建时报错
        // var versionPath = path.resolve(__dirname, 'core/version.txt');
        // fs.writeFileSync(versionPath,"version1");

        Generator.build_simple(config.autoPackVersion, 
            config.packageUrl, 
            target.buildPath+folder, 
            config.manifestUUID,
            function(err,res){
                Editor.log('[HotUpdateTools] 自动替换热更新文件结果:',err?'失败':'成功');
                if(err){
                    Editor.log('[HotUpdateTools] error:',err);
                }
                callback();
        });
    } else{
        Editor.log("[HotUpdateTools] 不自动替换热更文件！");
        callback();
    };
}
function addHotUpdateSearchPaths(target,callback){
    Editor.log("[HotUpdateTools] build platform:" + target.platform);
    if (target.platform === "win32" || target.platform === "android" || target.platform === "ios" || target.platform === "mac") {
        let root = path.normalize(target.dest);
        let url = path.join(root, "main.js");
        fs.readFile(url, "utf8", function (err, data) {
            if (err) {
                throw err;
            }
            let newStr =
                "else if (window.jsb) {\n"+
				"\n" +
                "    if (jsb) { \n" + 
                "        var hotUpdateSearchPaths = localStorage.getItem('HotUpdateSearchPaths'); \n" +
                "        if (hotUpdateSearchPaths) { \n" + 
                "            jsb.fileUtils.setSearchPaths(JSON.parse(hotUpdateSearchPaths)); \n" + 
                "        }\n" +
                "    }\n";
            // let newStr =
            //     "(function () { \n"+
			// 	"\n" +
            //     "    if (jsb) { \n" + 
            //     "        var hotUpdateSearchPaths = localStorage.getItem('HotUpdateSearchPaths'); \n" +
            //     "        if (hotUpdateSearchPaths) { \n" + 
            //     "            jsb.fileUtils.setSearchPaths(JSON.parse(hotUpdateSearchPaths)); \n" + 
            //     "        }\n" +
            //     "    }\n";
            //let newData = data.replace("(function () {", newStr);
            let newData = data.replace("else if (window.jsb) {", newStr);
            fs.writeFile(url, newData, function (error) {
                if (err) {
                    throw err;
                }
                Editor.log("[HotUpdateTools] 添加热更搜索路径成功");
                callback();
            });
        });
    } else {
        Editor.log("[HotUpdateTools] 不添加热更搜索路径, platform: " + target.platform);
        callback();
    }

    let time = new Date().getTime();
    // 通知panel更新时间
    Editor.Ipc.sendToPanel('hot-update-tools', 'hot-update-tools:onBuildFinished', time);
    // 写入本地
    let CfgUtil = Editor.require('packages://hot-update-tools/core/CfgUtil.js');
    CfgUtil.updateBuildTimeByMain(time);
}
module.exports = {
    load() {
        // 当 package 被正确加载的时候执行
        Editor.Builder.on('build-start', onBuildStart);
        Editor.Builder.on('before-change-files', onBeforeChangeFiles);
        Editor.Builder.on('build-finished', onBeforeBuildFinish);
    },

    unload() {
        // 当 package 被正确卸载的时候执行
        Editor.Builder.removeListener('build-start', onBuildStart);
        Editor.Builder.removeListener('before-change-files', onBeforeChangeFiles);
        Editor.Builder.removeListener('build-finished', onBeforeBuildFinish);
    },

    messages: {
        'showPanel'() {
            Editor.Panel.open('hot-update-tools');
        },
        'test'(event, args) {
            console.log("1111111");
            Editor.log(args);
            Editor.Ipc.sendToPanel('hot-update-tools', 'hot-update-tools:onBuildFinished');
        },
    },
};